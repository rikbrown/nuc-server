#!/bin/bash

# Chrome
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
sudo apt update
sudo apt -y install google-chrome-stable

# Standard tools
sudo apt -y install vim git fish curl
curl -L https://get.oh-my.fish | fish
chsh -s /usr/bin/fish

# Media center tools
sudo apt -y install docker.io docker-compose

# ssh server
sudo apt install ssh


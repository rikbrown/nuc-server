Uses docker and docker-compose to manage a full featured media server/fetcher for x86 platforms. Tested on an Intel NUC.

Most images from https://www.linuxserver.io/

# Requirements
```console
$ apt install docker.io docker-compose
```

# Running
In the same directory as the docker-compose.yml:

```console
$ docker-compose up
```

# NginxProxy

## Password protection

```console
htpasswd -c ~/MediaServer/config/nginx/htpasswd rik
```

## NzbHydra2

You need to modify the UrlBase in its configuration to `/nzbhydra2`.

# Notes
On Linux, after you start for the first time, but before setting up the services, make sure the folders are created with the correct permissions. I had to:

```console
chown -vR $USER downloads config media
```

## Network management
Use `nmcli` for Ubuntu 18.04.

## Troubleshooting
`ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?`

Make sure the docker daemon is running:

```console
$ sudo service docker status
```

Add your user to the docker group:

```console
$ sudo usermod -aG docker ${USER}
```

## How do I usenet?
https://www.reddit.com/r/usenet/wiki/index

## What sites do I use?
### Indexers
* NZBGeek - https://nzbgeek.info/
* 6box - https://6box.me/
* DrunkenSlug - https://drunkenslug.com/

### Providers
* Frugal Usenet - http://frugalusenet.com/
* Usenet Express - http://usenetexpress.com/
